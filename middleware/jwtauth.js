// import json webtoken 
const jwt = require('jsonwebtoken') 
 
const authJWT = (req, res, next) => { 
    try { 
        const authHeader = req.headers['authorization'] 
 
        console.log(req.headers) 
 
        if (authHeader) { 
            const token = authHeader.split(' ')[1]; 
            const secretKey = process.env.SECRET_KEY; 
            jwt.verify(token, secretKey, (err, user) => { 
                if (err) { 
                    return res.status(403).json({ 
                        message: err.message 
                    }) 
                } 
 
                req.user = user 
                next(); 
            }) 
        } else { 
            res.sendStatus(401) 
        } 
 
    } catch (error) { 
        res.status(401).json({ 
            message: 'Unauthorized' 
        }) 
    } 
} 
 
// export 
module.exports = authJWT