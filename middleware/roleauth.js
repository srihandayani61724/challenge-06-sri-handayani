// import json webtoken 
 
const jwt_decode = require('jwt-decode') 
 
const roleAuth = (req, res, next) => { 
    try { 
        const authHeader = req.headers['authorization'] 
 
        console.log(req.headers) 
 
        if (authHeader) { 
            const tokenPlus = authHeader.split(' ')[1] 
            console.log(tokenPlus) 
            const decoded = jwt_decode(tokenPlus) 
            console.log(decoded) 
            let role = decoded.role 
            if (role === "super admin" || role == "admin") { 
                next() 
            } else { 
                res.status(401).json({ 
                    message: 'Anda Bukan Rolenya!' 
                }) 
            } 
        } else { 
            res.sendStatus(403) 
        } 
 
    } catch (error) { 
        res.status(401).json({ 
            message: 'Tidak Memiliki Akses' 
        }) 
    } 
} 
 
// export 
module.exports = roleAuth 