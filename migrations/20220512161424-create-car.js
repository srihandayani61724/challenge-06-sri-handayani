'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Cars', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.DOUBLE
      },
      url_image:{
        type: Sequelize.TEXT
      },
      available: {
        type: Sequelize.BOOLEAN
      },
      size_id: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.BOOLEAN
      },
      updated_by: {
        type: Sequelize.BOOLEAN
      },
      deleted_by: {
        type: Sequelize.BOOLEAN
      },
      deleted_at: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Cars');
  }
};