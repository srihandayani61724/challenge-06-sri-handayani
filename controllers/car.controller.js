const { Car, Size } = require('../models');

//caecontroller
  async function getAllCar(req, res){
    try {
      const model_car = await Car.findAll({
        where: { deleted_at: null }
      });

      res.status(200).json({
        message: "Data mobil ditemukan",
        data:model_car
      })
    
    } catch (error) {
        res.status(500).json({
            message: error.message,
            data:null
        })
    }
  }
  async function getAllAvailableCar(req, res){
    try {
      const model_car = await Car.findAll({
        where: {
          deleted_at: null,
          available: true
        },
        includes: [{
          model: Size,
        }]
      });
      res.status(200).json({
        message: "Data mobil ditemukan",
        data:model_car
    })
    
} catch (error) {
    res.status(500).json({
        message: error.message,
        data:null
    })
}
}

async function  showCar(req, res){
    try {
      const model_car = await Car.findOne({
        where: {
          id: req.params.id,
          deleted_at: null
        },
        includes: [{
          model: Size,
        }]
      });
      res.status(200).json({
        message: "Data mobil ditemukan",
        data:model_car
    })
    
} catch (error) {
    res.status(500).json({
        message: error.message,
        data:null
    })
}
}


async function  createCar(req, res){
    try {
      const { name, price, url_image, available, size_id } = req.body;

      const model_car = await Car.create({
        name,
        price,
        url_image,
        size_id,
        available,
        createdAt: new Date(),
        updatedAt: new Date()
      });

      res.status(200).json({
        message: "Data mobil berhasil ditambahkan",
        data:model_car
    })
    
} catch (error) {
    res.status(500).json({
        message: error.message,
        data:null
    })
}
}
async function  updateCar(req, res){
    try {
      const { name, price, url_image, available, size_id } = req.body;

      const model_car = await Car.update(
        {
          name,
          price,
          url_image,
          available,
          size_id,
          updatedAt: new Date()
        }, {
          where: {id: req.params.id},
        })

        res.status(200).json({
            message: "Data mobil berhasil diubah",
            data:model_car
        })
        
    } catch (error) {
        res.status(500).json({
            message: error.message,
            data:null
        })
    }
    }

    async function softDeleteCar(req, res){
    try {
      const model_car = await Car.update({
        deleted_at: new Date(),
      }, {
        where: {id: req.params.id}
      })
      res.status(200).json({
        message: "Data mobil berhasil dihapus",
        data:model_car
    })
    
} catch (error) {
    res.status(500).json({
        message: error.message,
        data:null
    })
}
}
module.exports ={
getAllCar,
 getAllAvailableCar,
 showCar,
 createCar,
 updateCar,
 softDeleteCar  

}    







