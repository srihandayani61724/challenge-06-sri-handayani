
const {
    User
} = require('../models')
const bcrypt = require('bcryptjs')
require('dotenv').config()

//get all Admin
async function getallAdmin(req, res){
    try {
        const model_user = await User.findAll({
            where:{
                role:"admin"
            }
        })

        res.status(200).json({
            message: "data admin ditemukan",
            data:model_user
        })
        
    } catch (error) {
        res.status(500).json({
            message: error.message,
            data:null
        })
    }
}

async function showUser(req,res){
    try {
        const model_user = await User.findOne({
            where:{
                id: req.params.id,
                role:"admin"
            }
        })
        
        res.status(200).json({
            message:"data admin ditemukan",
            data:model_user
        })

    } catch (error) {
        res.status(500).json({
            message:eror.message,
            data:null
        })

    }
}
   async function createUser(req, res){
       try{
           const{
               name,
               email,
               password,
            }= req.body

        const salt = process.env.SALT;
        const hashPassword = await bcrypt.hash(password+salt, 12);

        const model_user = await User.create({
            name,
            email,
            password: hashPassword,
            role: "admin",
            updatedAt: new Date(),
            createdAt: new Date()
        })
        res.status(200).json({
            message:"data admin berhasil ditambahkan",
            data:model_user
        })
    
      }catch (eror) {
          res.status(500).json({
              message:eror.message,
              data:null
          })
      }
}
    async function updateUser(req, res){
        try{
            const{
                name,
                email,
                password,
             }= req.body
            
            const salt = process.env.SALT;
            const hashPassword = await bcrypt.hash(password+salt, 12);

            const model_user = await User.update({
                name,
                email,
                password: hashPassword,
                updatedAt: new Date()
            }, {
                where:{
                    id:req.params.id,
                    role:"admin"
                }
         })
        res.status(200).json({
            message:"Data admin berhasil diubah",
            data:model_user
        })
        }catch (eror) {
            res.status(500).json({
                message:eror.message,
                data:null
            })
        }
    }
 
    async function deletedUser(req, res){
        try{

            const model_user = await User.destroy({
                where:{
                    id:req.params.id,
                    role:"admin"
                }
            })
        res.status(200).json({
            message:"Data admin berhasil dihapus",
            data:model_user
        })
            }catch (eror){
                res.status(500).json({
                    message:eror.message,
                    date:null
                })
            }
        }
    module.exports ={
        getallAdmin,
        showUser,
        createUser,
        updateUser,
        deletedUser

    }    
    


