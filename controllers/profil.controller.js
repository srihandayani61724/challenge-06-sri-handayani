const jwt_decode = require('jwt-decode')
const { User } = require('../models');

async function currentUser(req, res){
    try {
      const header = req.headers['authorization'];
      const token = header.split(' ')[1];
      const decode = jwt_decode(token);
      const model_user = await User.findOne({
        where: { id:decode.id },
        attributes:{exclude:['password']}
      });


      res.status(200).json({
        message:"data user ditemukan",
        data:model_user
     })
    } catch (error) {
        res.status(500).json({
            message: error.message,
            data:null
        })
    }
  }
  module.exports ={
    currentUser

}    



