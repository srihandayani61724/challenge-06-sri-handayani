//import 
const bcrypt = require('bcryptjs') 
const jwt = require('jsonwebtoken') 
const db = require('./../models') 
require('dotenv').config() 
const { User } = require('../models');
async function login(req, res) { 
    try { 
        const body = req.body 
        if ((body.email !== '') && (body.password !== '')) { 
 
            //cari user 
            const user = await User.findOne({ 
                where: { 
                    email: body.email 
                } 
            }) 
 
            //match password 
            const matchPass = await bcrypt.compare(body.password + process.env.SALT, user.password) 
 
            if (!matchPass) { 
                return res.status(401).json({ 
                    messege: "username/password not match!" 
                }) 
            } 
            //jwt  
            const secretKey = process.env.SECRET_KEY 
            const expireKey = '2h' 
 
            const tokenAccess = jwt.sign({ 
                    id: user.id, 
                    name: user.name, 
                    role: user.role 
 
                }, 
                // secret key 
                secretKey, 
                //options 
                { 
                    algorithm: "HS256", 
                    expiresIn: expireKey 
                } 
            ) 
            return res.status(200).json({ 
                messege: "Success", 
                token: tokenAccess, 
                data: user 
            }) 
        } 
    } catch (error) { 
        console.log(error) 
    } 
} 

async function register(req, res){
    try {
      const { name, email, password} = req.body;
      
      const salt = process.env.SALT;
      const hashPassword = await bcrypt.hash(password+salt, 12);
      
      const body = {
        name,
        email,
        password: hashPassword,
        role: 'member',
        createdAt: new Date(),
        updatedAt: new Date()
      }

      const user =  await User.create(body)

      res.status(201).json({
        message: "pengguna berhasil didaftarkan",
        data:user
      })
    } catch (error) {
      res.status(500).json({
          message: error.message,
          data:null
        })
    }
  }

 
module.exports = { 
    login,
    register
}