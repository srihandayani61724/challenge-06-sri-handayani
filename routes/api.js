var express = require ('express');
var router = express.Router();
const authJWT = require('../middleware/jwtauth') 
const rolAuth = require('../middleware/roleauth')
const isSuperAdmin = require('../middleware/isSuperAdmin')
//import login
const {login, register} = require ('../controllers/auth.controller')
//import admin
const{
    getallAdmin,
    showUser,
    createUser,
    updateUser,
    deletedUser
}= require('../controllers/admin.controller')

//import
const{
    getAllCar,
    getAllAvailableCar,
    showCar,
    createCar,
    updateCar,
    softDeleteCar   
}= require('../controllers/car.controller')

//import
const{
    currentUser,
}= require('../controllers/profil.controller')

router.post('/v1/login', login)
router.post('/v1/register', register)

//Get Users
router.get('/v1/admin', [authJWT, isSuperAdmin], getallAdmin)
router.get('/v1/admin/:id', [authJWT, isSuperAdmin], showUser)
router.post('/v1/admin', [authJWT, isSuperAdmin], createUser)
router.put('/v1/admin/:id', [authJWT, isSuperAdmin], updateUser)
router.delete('/v1/admin/:id', [authJWT, isSuperAdmin], deletedUser)

router.get('/v1/cars/available', [authJWT, rolAuth], getAllAvailableCar)
router.get('/v1/cars', [authJWT, rolAuth], getAllCar)
router.get('/v1/cars/:id', [authJWT, rolAuth], showCar)
router.post('/v1/cars', [authJWT, rolAuth], createCar)
router.put('/v1/cars/:id', [authJWT, rolAuth], updateCar)
router.delete('/v1/cars/:id', [authJWT, rolAuth], softDeleteCar)

router.get('/v1/currentUser', authJWT, currentUser)

module.exports = router;


